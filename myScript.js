function loadXMLDoc(theURL)
{
	// Get as a string all the JASON from SWAPI

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari, SeaMonkey
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            //alert(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", theURL, false);
    xmlhttp.send();
    return xmlhttp.responseText; 
}

function myFunction(){
	var url = "http://swapi.co/api/people/";
	var text = "";

	var names = getNames(url);

	for (var i = 0; i < names.length; i++) {
		text += names[i]+"<br />";
	};
	document.getElementById("demo").innerHTML = text;
}

function myFunctionAll(){
	var url = "http://swapi.co/api/people/";
	var text = "";

	var names = getAllNames(url);

	for (var i = 0; i < names.length; i++) {
		text += names[i]+"<br />";
	};
	document.getElementById("demo").innerHTML = text;
}


function getAllNames (url){
	var urlData;
	var obj;
	var names = [];
	while (url != null){
		urlData = loadXMLDoc(url);
		obj = JSON.parse(urlData);
		names = names.concat(getNames(url));
		url = obj.next;
	}
	console.log(names.length);
	return names;
}

function getNames( url) {
	var names = [];
	var urlData = loadXMLDoc(url);
	var obj = JSON.parse(urlData);
  	for (var i = 0 ; i < obj.results.length; i++) {
  		names.push(obj.results[i].name);
  	};
  	return names;
}



